"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var registration_service_1 = require("../registration.service");
var constants_1 = require("../constants");
var ForgotFormComponent = /** @class */ (function () {
    function ForgotFormComponent(service) {
        this.service = service;
        this.otpMaxLength = constants_1.default.otpMaxLength;
        this.showPass = false;
        this.wrongOTP = false;
        this.otpVerified = new core_1.EventEmitter();
        this.OTPCodeInput = new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.minLength(6), forms_1.Validators.maxLength(6)]);
        this.OTPCodeInput.valueChanges.subscribe(this.OTPChange.bind(this));
    }
    ForgotFormComponent.prototype.keyboardInput = function (event) {
        if (event.keyCode === 13) {
            if (this.OTPCodeInput.valid) {
                this.sendOtp();
            }
        }
    };
    ForgotFormComponent.prototype.OTPChange = function () {
        this.wrongOTP = false;
    };
    ForgotFormComponent.prototype.togglePassShow = function () {
        this.showPass = !this.showPass;
    };
    Object.defineProperty(ForgotFormComponent.prototype, "attempts", {
        get: function () {
            var attemptsText = '';
            switch (this.verification_attempts) {
                case 1:
                    attemptsText = 'Осталась 1 попытка';
                    break;
                case 2:
                    attemptsText = 'Осталось 2 попытки';
                    break;
                case 3:
                    attemptsText = 'Осталось 3 попытки';
                    break;
                default:
                    break;
            }
            return attemptsText;
        },
        enumerable: true,
        configurable: true
    });
    ForgotFormComponent.prototype.sendOtp = function () {
        var _this = this;
        if (this.OTPCodeInput.valid && this.verification_attempts !== 0) {
            var otpCheck$ = this.service.checkOTPCode(this.OTPCodeInput.value, this.verification_guid);
            otpCheck$.subscribe(function (results) {
                if (results.status === 'Y') {
                    _this.otpVerified.emit(true);
                    _this.wrongOTP = false;
                }
                else {
                    _this.otpVerified.emit(false);
                    _this.wrongOTP = true;
                }
                _this.verification_attempts = results.verification_attempts;
            });
        }
    };
    ForgotFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], ForgotFormComponent.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ForgotFormComponent.prototype, "verification_guid", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ForgotFormComponent.prototype, "otpVerified", void 0);
    __decorate([
        core_1.HostListener('window:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ForgotFormComponent.prototype, "keyboardInput", null);
    ForgotFormComponent = __decorate([
        core_1.Component({
            selector: 'app-forgot-pass-form',
            template: "\n    <div class=\"reg-form-wrapper reg-otp-form\">\n      <div class=\"reg-register-form__img-holder\">\n        <div class=\"reg-icon-img\"><img src=\"../assets/images/phone.svg\" alt=\"\"></div>\n        <div class=\"reg-main-img\"><img src=\"../assets/images/register-pass.png\" alt=\"\"></div>\n        <div class=\"reg-icon-img\"><img src=\"../assets/images/lock-plus.svg\" alt=\"\"></div>\n      </div>\n      <div class=\"reg-actions-block__sms-confirm\">\n        <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': wrongOTP }\">\n          <input id=\"sms-password\"\n                 [formControl]=\"OTPCodeInput\"\n                 [maxlength]=\"otpMaxLength\"\n                 autocomplete=\"off\"\n                 autofocus\n                 required\n                 [type]=\"showPass ? 'text' : 'password'\"\n                 #autofocusInput\n          >\n          <label for=\"sms-password\">{{'sms_password' | translate}}</label>\n          <span class=\"reg-pass-icon\" (click)=\"togglePassShow()\">\n        <img [src]=\"showPass ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'\" alt=\"\">\n      </span>\n          <div class=\"reg-error-field\" *ngIf=\"wrongOTP && verification_attempts !== 0\"> {{'sms_password_wrong' | translate}}</div>\n          <div class=\"reg-error-field\" *ngIf=\"verification_attempts === 0\">\n            {{'verification_attempts_zero' | translate}}\n          </div>\n        </div>\n        <div class=\"reg-form-info\">{{'enter_sms_password' | translate}}</div>\n        <div class=\"reg-attempts\">{{attempts}}</div>\n        <button class=\"reg-button\"  (click)=\"sendOtp()\" [disabled]=\"!OTPCodeInput.valid\">{{'confirm' | translate}}</button>\n\n      </div>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService])
    ], ForgotFormComponent);
    return ForgotFormComponent;
}());
exports.ForgotFormComponent = ForgotFormComponent;
//# sourceMappingURL=forgot-pass-form.component.js.map