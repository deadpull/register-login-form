import { EventEmitter } from '@angular/core';
export interface IOptions {
    maxWidth: number;
    maxHeight: number;
}
export declare class AvatarComponent {
    src: string;
    editable: boolean;
    error: string;
    options: IOptions;
    onChange: EventEmitter<string>;
    constructor();
    selectAvatar(): void;
    private openFileDialog(options?);
}
