"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var ChangePasswordFormComponent = /** @class */ (function () {
    function ChangePasswordFormComponent() {
        this.wrongPassword = false;
        this.passwordMismatch = false;
        this.onSubmit = new core_1.EventEmitter();
        this.onCancel = new core_1.EventEmitter();
        this.formGroup = new forms_1.FormGroup({
            currPassword: new forms_1.FormControl('', forms_1.Validators.required),
            newPassword: new forms_1.FormControl('', forms_1.Validators.required),
            confirmPassword: new forms_1.FormControl('', forms_1.Validators.required)
        }, this.checkPasswords.bind(this));
    }
    ChangePasswordFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    ChangePasswordFormComponent.prototype.validateControl = function (key) {
        var control = this.formGroup.controls[key];
        return !control || !!(control.valid || !control.touched);
    };
    ChangePasswordFormComponent.prototype.submit = function () {
        if (!this.formGroup.valid) {
            return;
        }
        var _a = this.formGroup.value, currPassword = _a.currPassword, newPassword = _a.newPassword;
        this.onSubmit.emit({ change_pass: {
                current_pass: currPassword,
                new_pass: newPassword
            } });
    };
    ChangePasswordFormComponent.prototype.close = function () {
        this.onCancel.emit();
    };
    ChangePasswordFormComponent.prototype.checkPasswords = function (group) {
        var _a = group.value, newPassword = _a.newPassword, confirmPassword = _a.confirmPassword;
        var valid = (!newPassword || !confirmPassword || newPassword === confirmPassword);
        this.passwordMismatch = !valid;
        return valid ? null : { areEqual: true };
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], ChangePasswordFormComponent.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ChangePasswordFormComponent.prototype, "wrongPassword", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ChangePasswordFormComponent.prototype, "onSubmit", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ChangePasswordFormComponent.prototype, "onCancel", void 0);
    ChangePasswordFormComponent = __decorate([
        core_1.Component({
            selector: 'app-change-password-form',
            template: "\n    <form class=\"reg-form-wrapper\" [formGroup]=\"formGroup\" (ngSubmit)=\"submit()\">\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !validateControl('currPassword')}\">\n        <input\n          id=\"currPassword\"\n          autocomplete=\"off\"\n          autofocus\n          required\n          type=\"password\"\n          formControlName=\"currPassword\"\n          #autofocusInput\n        >\n        <label for=\"currPassword\">{{'old_password' | translate}}</label>\n        <div class=\"reg-error-field\" *ngIf=\"wrongPassword\">\n          {{'password_wrong' | translate}}\n        </div>\n      </div>\n\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': passwordMismatch}\">\n        <input\n          id=\"newPassword\"\n          autocomplete=\"off\"\n          required\n          type=\"password\"\n          formControlName=\"newPassword\"\n        >\n        <label for=\"newPassword\">{{'new_password' | translate}}</label>\n      </div>\n\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': passwordMismatch}\">\n        <input\n          id=\"confirmPassword\"\n          autocomplete=\"off\"\n          required\n          type=\"password\"\n          formControlName=\"confirmPassword\"\n        >\n        <label for=\"confirmPassword\">{{'repeat_new_password' | translate}}</label>\n      </div>\n\n      <div class=\"reg-error-field\" *ngIf=\"passwordMismatch\">\n        {{'password_mismatch' | translate}}\n      </div>\n      \n      <div class=\"button-block\">\n        <button type=\"button\" (click)=\"close()\">{{'cancel' | translate}}</button>\n        <button type=\"submit\" class=\"reg-button\" [disabled]=\"!formGroup.valid\">{{'confirm' | translate}}</button>\n      </div>\n    </form>\n  ",
        }),
        __metadata("design:paramtypes", [])
    ], ChangePasswordFormComponent);
    return ChangePasswordFormComponent;
}());
exports.ChangePasswordFormComponent = ChangePasswordFormComponent;
//# sourceMappingURL=change-password-form.component.js.map