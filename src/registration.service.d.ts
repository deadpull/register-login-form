import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IUser, IServiceConfig, IUserCheckResult, IOTPCheckResult } from './interfaces';
export declare class RegistrationService {
    private http;
    config: IServiceConfig;
    constructor(http: Http);
    getUserAuthInfo(phoneNumber: string): Observable<IUserCheckResult>;
    forgotPassGetUser(phoneNumber: string): Observable<IUserCheckResult>;
    checkOTPCode(otpCode: string, verification_guid: string): Observable<IOTPCheckResult>;
    login(password: string, msisdn: string): Observable<any>;
    registerNewPassForgot(password: string, msisdn: string, verification_guid?: string): Observable<boolean>;
    register(password: string, msisdn: string, verification_guid?: string): Observable<boolean>;
    editUser(user: IUser, updatedata: any): Observable<any>;
    getProfile(user: IUser): Observable<any>;
    getUserAvatar(user: IUser, size?: number): string;
}
