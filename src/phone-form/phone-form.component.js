"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var registration_service_1 = require("../registration.service");
var PhoneFormComponent = /** @class */ (function () {
    function PhoneFormComponent(service) {
        this.service = service;
        this.phone = new core_1.EventEmitter();
        this.userCredentials = new core_1.EventEmitter();
        this.userExistsEvent = new core_1.EventEmitter();
        this.userCheckDone = false;
        this.showError = false;
        this.userData = null;
        this.latestValue = '';
        this.userExists = false;
        this.phoneInput = new forms_1.FormControl('', [forms_1.Validators.required,
            forms_1.Validators.pattern(/^\+380(39|50|63|66|67|68|91|92|93|94|95|96|97|98|99)\d{7}/)]);
    }
    PhoneFormComponent.prototype.keyboardInput = function (event) {
        if (event.keyCode === 13) {
            if (this.userData) {
                this.userExists ? this.confirmUser() : this.sendOtp();
            }
        }
    };
    Object.defineProperty(PhoneFormComponent.prototype, "userExists", {
        get: function () {
            return this._userExists;
        },
        set: function (value) {
            this._userExists = value;
            this.userExistsEvent.emit(value);
        },
        enumerable: true,
        configurable: true
    });
    ;
    PhoneFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    PhoneFormComponent.prototype.sendOtp = function () {
        if (this.userData) {
            this.userCredentials.emit(this.userData);
        }
    };
    PhoneFormComponent.prototype.enableErrorField = function () {
        if (this.phoneInput.value && this.phoneInput.value.length === 13) {
            return !this.phoneInput.valid;
        }
        return false;
    };
    PhoneFormComponent.prototype.valueChange = function () {
        if (this.latestValue === this.phoneInput.value) {
            return;
        }
        this.latestValue = this.phoneInput.value;
        this.userCheckDone = false;
        this.userData = null;
        this.userExists = false;
        this.phone.emit('');
        this.showError = false;
        if (this.phoneInput.value && this.phoneInput.value.length === 13) {
            this.showError = !this.phoneInput.valid;
        }
        if (this.phoneInput.valid) {
            this.phone.emit(this.phoneInput.value);
            this.checkUserExists(this.phoneInput.value);
        }
    };
    PhoneFormComponent.prototype.checkUserExists = function (number) {
        var _this = this;
        var userCheckResults$ = this.service.getUserAuthInfo(number);
        userCheckResults$.subscribe(function (results) {
            _this.userCheckDone = true;
            if (!results) {
                _this.userExists = false;
                _this.userData = null;
                return;
            }
            _this.userExists = results.userExists;
            _this.userData = results;
        });
    };
    PhoneFormComponent.prototype.confirmUser = function () {
        this.userCredentials.emit(this.userData);
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], PhoneFormComponent.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PhoneFormComponent.prototype, "action", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PhoneFormComponent.prototype, "lang", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PhoneFormComponent.prototype, "phone", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PhoneFormComponent.prototype, "userCredentials", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PhoneFormComponent.prototype, "userExistsEvent", void 0);
    __decorate([
        core_1.HostListener('window:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], PhoneFormComponent.prototype, "keyboardInput", null);
    PhoneFormComponent = __decorate([
        core_1.Component({
            selector: 'app-phone-form',
            template: "\n    <div class=\"reg-form-wrapper reg-phone-form\">\n      <div class=\"reg-register-form__img-holder\">\n        <div class=\"reg-main-img\"><img src=\"assets/images/blue-logo.svg\" alt=\"\"></div>\n        <div *ngIf=\"action==='login' && !(userCheckDone && !userExists)\" class=\"reg-icon-img lock-state\">\n          <!--<img src=\"assets/images/lock.svg\" alt=\"\">-->\n        </div>\n        <div *ngIf=\"action==='register' || (action==='login' && (userCheckDone && !userExists))\" class=\"reg-icon-img\">\n          <img src=\"assets/images/message.svg\" alt=\"\"></div>\n        <div *ngIf=\"action==='register' || (action==='login' && (userCheckDone && !userExists))\" class=\"reg-icon-img\">\n          <!--<img src=\"assets/images/lock-plus.svg\" alt=\"\">--></div>\n      </div>\n      <div class=\"reg-input-holder reg-simple-field reg-phone-field\" [ngClass]=\"{'error-input': showError}\">\n        <input id=\"phone\"\n               type=\"tel\"\n               phone\n               (valueChanges)=\"valueChange()\"\n               [formControl]=\"phoneInput\"\n               required\n               #autofocusInput\n        >\n        <label for=\"phone\">{{'phone_number' | translate}}</label>\n        <span *ngIf=\"userExists\" class=\"reg-pass-icon\">\n        <img src=\"../assets/images/done.svg\" alt=\"\">\n      </span>\n        <div class=\"reg-error-field\" *ngIf=\"showError\">\n          {{'wong_phone_number' | translate}}\n        </div>\n      </div>\n      <div *ngIf=\"action==='register' && !userExists\" class=\"reg-form-info\">\n        {{'phone_not_registered_hint' | translate}}\n      </div>\n      <div class=\"reg-form-info icon-on-side\">\n       <img src=\"assets/images/sm-lock.svg\"> <span>{{'phone_registered_hint1' | translate}}</span>\n      </div>\n      <div class=\"reg-form-info icon-on-side\">\n       <img src=\"assets/images/add-user.svg\"> <span>{{'phone_registered_hint2' | translate}}</span>\n      </div>\n      <div *ngIf=\"userExists && action==='register'\" class=\"reg-form-info\">\n        {{'phone_already_registered' | translate }}\n      </div>\n      <div class=\"controls\">\n        <button class=\"reg-button\" [disabled]=\"!userData\" (click)=\"userExists ? confirmUser(): sendOtp()\">\n          {{userExists && action === 'register' ? ('enter_password' | translate) : ('proceed' | translate)}}\n        </button>\n      </div>\n    </div>\n  ",
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService])
    ], PhoneFormComponent);
    return PhoneFormComponent;
}());
exports.PhoneFormComponent = PhoneFormComponent;
//# sourceMappingURL=phone-form.component.js.map