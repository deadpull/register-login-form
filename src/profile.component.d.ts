import { OnInit, OnChanges, EventEmitter } from '@angular/core';
import { RegistrationService } from './registration.service';
import { IUser, IServiceConfig } from './interfaces';
import { LocaleService } from './locale.service';
import { ILocales } from './interfaces';
export interface IProfileOptions {
    buttons?: {
        editAvatar?: boolean;
        editInfo?: boolean;
        editPassword?: boolean;
    };
}
export declare class ProfileComponent implements OnInit, OnChanges {
    private service;
    private localeService;
    user: IUser;
    lang: string;
    config: IServiceConfig;
    locales: ILocales;
    options: IProfileOptions;
    onLogout: EventEmitter<void>;
    onClose: EventEmitter<void>;
    onUserUpdate: EventEmitter<IUser>;
    hidden: boolean;
    state: '' | 'changePassword' | 'edit';
    msisdn: string;
    avatarSrc: string;
    wrongPassword: boolean;
    avatarError: string;
    constructor(service: RegistrationService, localeService: LocaleService);
    ngOnInit(): void;
    ngOnChanges(changes: any): void;
    changePassword(): void;
    edit(): void;
    logout(): void;
    close(): void;
    editProfile(data: any): void;
    formatPhone(): void;
    onAvatarChange(data: any): void;
    private showAvatarSizeError();
}
