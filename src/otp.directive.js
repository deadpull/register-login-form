"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var mask_worker_1 = require("./mask.worker");
function formatPhone(value) {
    if (value === void 0) { value = ''; }
    var maskWorker = new mask_worker_1.MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]');
    return maskWorker.useMask(value);
}
exports.formatPhone = formatPhone;
var OTPDirective = /** @class */ (function () {
    function OTPDirective(model, el) {
        var _this = this;
        this.model = model;
        this.el = el;
        this.valueChanges = new core_1.EventEmitter();
        this.maskWorker = new mask_worker_1.MaskWorker('XXXXXX', 'XXXXXX', '[0-9]', function (otpString) {
            if (otpString.length > 6) {
                return otpString.slice(0, 6);
            }
            return otpString;
        });
        this.valueChanges.subscribe(function (phone) {
            if (_this.formControl) {
                _this.formControl.setValue(phone, {
                    onlySelf: true,
                    emitEvent: false,
                    emitModelToViewChange: false,
                    emitViewToModelChange: false
                });
            }
        });
    }
    OTPDirective.prototype.ngOnInit = function () {
        if (this.formControl) {
            this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
        }
    };
    OTPDirective.prototype.onInputChange = function (event) {
        var start = this.el.nativeElement.selectionStart;
        var maskedValue = this.maskWorker.useMask(event);
        this.model.valueAccessor.writeValue(maskedValue);
        this.el.nativeElement.selectionStart = start;
        this.el.nativeElement.selectionEnd = start;
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.AbstractControl)
    ], OTPDirective.prototype, "formControl", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], OTPDirective.prototype, "valueChanges", void 0);
    OTPDirective = __decorate([
        core_1.Directive({
            selector: '[otp]',
            providers: [forms_1.NgModel],
            host: {
                '(ngModelChange)': 'onInputChange($event)'
            }
        }),
        __metadata("design:paramtypes", [forms_1.NgModel, core_1.ElementRef])
    ], OTPDirective);
    return OTPDirective;
}());
exports.OTPDirective = OTPDirective;
//# sourceMappingURL=otp.directive.js.map