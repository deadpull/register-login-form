import { Component, Input, Output, EventEmitter, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password-form',
  template: `
    <form class="reg-form-wrapper" [formGroup]="formGroup" (ngSubmit)="submit()">
      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !validateControl('currPassword')}">
        <input
          id="currPassword"
          autocomplete="off"
          autofocus
          required
          type="password"
          formControlName="currPassword"
          #autofocusInput
        >
        <label for="currPassword">{{'old_password' | translate}}</label>
        <div class="reg-error-field" *ngIf="wrongPassword">
          {{'password_wrong' | translate}}
        </div>
      </div>

      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': passwordMismatch}">
        <input
          id="newPassword"
          autocomplete="off"
          required
          type="password"
          formControlName="newPassword"
        >
        <label for="newPassword">{{'new_password' | translate}}</label>
      </div>

      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': passwordMismatch}">
        <input
          id="confirmPassword"
          autocomplete="off"
          required
          type="password"
          formControlName="confirmPassword"
        >
        <label for="confirmPassword">{{'repeat_new_password' | translate}}</label>
      </div>

      <div class="reg-error-field" *ngIf="passwordMismatch">
        {{'password_mismatch' | translate}}
      </div>
      
      <div class="button-block">
        <button type="button" (click)="close()">{{'cancel' | translate}}</button>
        <button type="submit" class="reg-button" [disabled]="!formGroup.valid">{{'confirm' | translate}}</button>
      </div>
    </form>
  `,
})
export class ChangePasswordFormComponent implements OnInit {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  @Input()
  public wrongPassword: boolean = false;

  @Output()
  public onSubmit: EventEmitter<any>;

  @Output()
  public onCancel: EventEmitter<any>;

  public formGroup: FormGroup;

  public passwordMismatch: boolean = false;

  public constructor() {
    this.onSubmit = new EventEmitter<any>();
    this.onCancel = new EventEmitter<any>();

    this.formGroup = new FormGroup({
      currPassword: new FormControl('', Validators.required),
      newPassword: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
    }, this.checkPasswords.bind(this));
  }

  ngOnInit() {
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }

  public validateControl(key: string): boolean {
    const control = this.formGroup.controls[key];
    return !control || !!(control.valid || !control.touched);
  }

  public submit(): void {
    if (!this.formGroup.valid) {
      return;
    }

    const {currPassword, newPassword} = this.formGroup.value;
    this.onSubmit.emit({change_pass: {
      current_pass: currPassword,
      new_pass: newPassword
    }});
  }

  public close(): void {
    this.onCancel.emit();
  }

  public checkPasswords(group: FormGroup): any {
    const {newPassword, confirmPassword} = group.value;
    const valid = (!newPassword || !confirmPassword || newPassword === confirmPassword);
    this.passwordMismatch = !valid;
    return valid ? null : {areEqual: true};
  }
}
