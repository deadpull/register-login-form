export declare class MaskWorker {
    private mask;
    private output;
    private allowedInputs;
    private advancedSetter;
    private replacement;
    private allowedSymbols;
    constructor(mask: any, output: any, allowedInputs: string, advancedSetter?: any, replacement?: string);
    private removeNotAllowedSymbolsFromString(inputString?, ignoreReplacement?);
    private restoreToTextOutput(string);
    private getMeaningPart(string);
    private removePredefinedOutput(string);
    private cutTail(string);
    private useTextMask(string);
    private applyAdvancedSetter(string);
    useMask(string?: string): any;
    unmask(string?: string): any;
}
