import { PipeTransform } from '@angular/core';
import { LocaleService } from "../locale.service";
export declare class TranslatePipe implements PipeTransform {
    private localeService;
    constructor(localeService: LocaleService);
    transform(value: any, ...args: any[]): any;
}
