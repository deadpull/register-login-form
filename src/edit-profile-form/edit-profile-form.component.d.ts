import { EventEmitter, OnInit, OnChanges, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IUser } from '../interfaces';
export declare class EditProfileForm implements OnInit, OnChanges {
    autofocusInputRef: ElementRef;
    user: IUser;
    onSubmit: EventEmitter<any>;
    onCancel: EventEmitter<any>;
    formGroup: FormGroup;
    constructor();
    ngOnInit(): void;
    ngOnChanges(): void;
    initFormValues(): void;
    validateControl(key: string): boolean;
    submit(): void;
    close(): void;
}
