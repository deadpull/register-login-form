import { Pipe, PipeTransform } from '@angular/core';

import { LocaleService } from "../locale.service";

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private localeService: LocaleService) {
  }

  transform(value: any, ...args): any {
    return this.localeService.translate(value, args);
  }
}
