import { Injectable } from '@angular/core';
import locales from './locales';

@Injectable()
export class LocaleService {
  private allowedLangs = ['ru', 'uk', 'en'];
  private currentLang = 'ru';

  private locales: any;

  constructor() {
    this.locales = locales;
  }

  translate(value, args) {
    let currentValue = this.locales[this.currentLang][value] || value;

    args.forEach((arg, index) => {
      currentValue = currentValue.replace(`%S${index+1}`, arg)
    });

    return currentValue;
  }

  updateLocales(customLocales = {}) {
    for (let locale in customLocales) {
      if (customLocales.hasOwnProperty(locale)) {
        this.locales[locale] = Object.assign({}, this.locales[locale], customLocales[locale])
      }
    }
  }

  public setLocale(lang: string) {
    if (this.allowedLangs.indexOf(lang) === -1) {
      return
    }
    this.currentLang = lang;
  }
}