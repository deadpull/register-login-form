"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    enter_to: 'Вхід до',
    registration_in: 'Регістрація в',
    phone_number: 'Номер телефону',
    phone_not_registered_hint: 'Введіть номер телефону, котрий буде вашим логіном для входу до сервису',
    phone_registered_hint: 'Введите номер телефона, который зарегистрирован в системе',
    phone_not_registered_result: 'Номер не зарегистрирован, нажмите "Продолжить" для регистрации',
    phone_already_registered: 'Этот номер уже зарегистрирован в нашей системе, введите пароль, чтобы войти',
    enter_password: 'Ввести пароль',
    enter_password_request: 'Введите пароль',
    proceed: 'Продолжить',
    set_password: 'Установите пароль',
    repeat_password: 'Повторите пароль',
    set_password_using_symbols: 'Установите пароль. Используйте латинские символы и цифры',
    sms_password: 'ОТП - пароль',
    sms_password_wrong: 'Некорректный OTP-пароль телефона.',
    verification_attempts_zero: 'Вы исчерпали количество попыток ввода, попробуйте позже',
    enter_sms_password: 'Введите OTP-пароль из СМС',
    confirm: 'Подтвердить',
    login: 'Войти',
    register_success_congratulations: 'Поздравляем с успешной регистрацией в'
};
//# sourceMappingURL=ua.js.map