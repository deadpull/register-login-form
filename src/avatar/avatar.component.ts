import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs/Observable';

export interface IOptions {
  maxWidth: number;
  maxHeight: number;
};

@Component({
  selector: 'app-avatar',
  template: `
    <div class="avatar" (click)="selectAvatar()">
      <div *ngIf="editable" class="avatar-overlay"></div>
      <div *ngIf="error" class="avatar-error-overlay">
        <div class="message">{{error | translate}}</div>
      </div>
      <img *ngIf="!src" src="../assets/images/blue-user.svg" alt="">  
      <img *ngIf="src" [src]="src" alt="">  
    </div>
  `
})
export class AvatarComponent {
  @Input()
  public src: string;

  @Input()
  public editable: boolean = true;

  @Input()
  public error: string = '';

  @Input()
  public options: IOptions = {
    maxWidth: 640,
    maxHeight: 640
  };

  @Output()
  public onChange: EventEmitter<string>;

  public constructor() {
    this.onChange = new EventEmitter<string>();
  }

  public selectAvatar(): void {
    if (!this.editable) {
      return;
    }

    this.openFileDialog({accept: 'image/*'})
      .map((data) => data.target.result)
      .flatMap((buffer) => {
        if (!/^data\:image/.test(buffer)) {
          return Observable.of(null);
        }

        const img = new Image();
        setTimeout(() => img.src = buffer);

        return Observable.fromEvent(img, 'load')
          .map(() => (
            img.width <= this.options.maxWidth
              && img.height <= this.options.maxHeight
            ) ? buffer : null
          );
      })
      .subscribe((buffer) => this.onChange.emit(buffer));
  }

  private openFileDialog(options: {
    accept?: string;
  } = {}): Observable<any> {
    const inputElement: HTMLInputElement = document.createElement('input');
    inputElement.type = 'file';
    inputElement.accept = options.accept || '';

    setTimeout(() => inputElement.click())
    return Observable.fromEvent(inputElement, 'change')
      .flatMap((event: any) => {
        const file = inputElement.files[0];

        if (!file) {
          return Observable.throw({message: 'invalid file'});
        }

        const reader = new FileReader();

        setTimeout(() => reader.readAsDataURL(file));
        return Observable.fromEvent(reader, 'load');
      });
  }
}
