import { EventEmitter, OnInit, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
export declare class ChangePasswordFormComponent implements OnInit {
    autofocusInputRef: ElementRef;
    wrongPassword: boolean;
    onSubmit: EventEmitter<any>;
    onCancel: EventEmitter<any>;
    formGroup: FormGroup;
    passwordMismatch: boolean;
    constructor();
    ngOnInit(): void;
    validateControl(key: string): boolean;
    submit(): void;
    close(): void;
    checkPasswords(group: FormGroup): any;
}
