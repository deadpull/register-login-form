import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RegistrationService } from "../registration.service";
export declare class OtpFormComponent implements OnInit {
    private service;
    autofocusInputRef: ElementRef;
    otpMaxLength: number;
    showPass: boolean;
    wrongOTP: boolean;
    verification_attempts: number;
    OTPCodeInput: AbstractControl;
    verification_guid: string;
    otpVerified: EventEmitter<boolean>;
    forgotPassEvent: EventEmitter<boolean>;
    readonly attempts: string;
    constructor(service: RegistrationService);
    keyboardInput(event: KeyboardEvent): void;
    OTPChange(): void;
    togglePassShow(): void;
    sendOtp(): void;
    ngOnInit(): void;
}
