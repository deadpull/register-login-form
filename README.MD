# mosst-registration

## Installation

To install this library, run:

```bash
$ npm install git+https://gitlab.com/ByteInBites/register-login-form.git --save
```

and then from your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import your library
import { RegistrationModule } from 'mosst-registration';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify your library as an import
    RegistrationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once your library is imported, you can use its components, directives and pipes in your Angular application:

```xml
<!-- You can now use your library component in app.component.html -->
<h1>
  {{title}}
</h1>
<registration [config]="config" 
  (user)="getUser($event)" 
  (registrationDone)="registrationDone()">
   (onClose)="closeAction()" 
   [action]="'register' or 'login'"
   [systemName]="'MOSST payments'"
   [lang]="currentLang" // 'en','ru','ua; 
   [locales]="locales" // если надо на ходу переписать текстовку - смотришь в исходниках как обозвано нужное поле, 
   // и передаешь объект, который его перекрывает :   locales = {
                                                                en: {
                                                                  enter_to: 'Enter to'
                                                                  ....
                                                                },
                                                                ru: {
                                                                  enter_to: 'Войти в'
                                                                }
                                                              };
  </registration> // 
```


 In component, where you use this library define config in this o similar way:
``` typescript
    config = {
      registrationUrl: '/api/user/register',
      otpCheckUrl: '/api/user/otp',
      loginUrl: '/api/user/login'
    }
    
    getUser(user){
      this.user = user;
    }
```

MIT © [rick](mailto:seniagin2012@gmail.com)
