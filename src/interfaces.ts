export interface IUser {
  profile_session_guid: string;
  profile_guid: string;
  valid_dtm: string;
  msisdn?: string;
  first_name?: string;
  last_name?: string;
  middle_name?: string;
  avatar_path?: string;
  mosst_tag?: string;
  gender?: string;
  emails?: string[];
  birth_dt?: string;
  biometric?: string;
  notification_type?: 'E' | 'S' | 'ES';
}
export interface IServiceConfig {
  loginUrl: string;
  forgotUrl: string;
  registrationUrl: string;
  otpCheckUrl: string;
  profileUrl?: string;
  avatarUrl?: string;
}

export interface IUserCheckResult {
  userExists: boolean;
  verification_guid?: string;
  verification_attempts?: number;
}
export interface IOTPCheckResult {
  status?: string;
  verification_attempts?: number;
}

export interface ILocales {
  ru?: any;
  en?: any;
  ua?: any;
}
