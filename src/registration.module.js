"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var registration_component_1 = require("./registration.component");
var profile_component_1 = require("./profile.component");
var otp_form_component_1 = require("./otp-form/otp-form.component");
var password_form_component_1 = require("./password-form/password-form.component");
var login_form_component_1 = require("./login-form/login-form.component");
var forgot_pass_form_component_1 = require("./forgot-pass-form/forgot-pass-form.component");
var phone_form_component_1 = require("./phone-form/phone-form.component");
var change_password_form_component_1 = require("./change-password-form/change-password-form.component");
var edit_profile_form_component_1 = require("./edit-profile-form/edit-profile-form.component");
var avatar_component_1 = require("./avatar/avatar.component");
var phone_directive_1 = require("./phone.directive");
var registration_service_1 = require("./registration.service");
var locale_service_1 = require("./locale.service");
var translate_1 = require("./pipes/translate");
var error_service_1 = require("./error.service");
var RegistrationModule = /** @class */ (function () {
    function RegistrationModule() {
    }
    RegistrationModule = __decorate([
        core_1.NgModule({
            declarations: [
                phone_directive_1.PhoneDirective,
                registration_component_1.RegistrationComponent,
                profile_component_1.ProfileComponent,
                otp_form_component_1.OtpFormComponent,
                forgot_pass_form_component_1.ForgotFormComponent,
                password_form_component_1.PasswordFormComponent,
                login_form_component_1.LoginFormComponent,
                phone_form_component_1.PhoneFormComponent,
                change_password_form_component_1.ChangePasswordFormComponent,
                edit_profile_form_component_1.EditProfileForm,
                avatar_component_1.AvatarComponent,
                translate_1.TranslatePipe
            ],
            providers: [
                error_service_1.ErrorService,
                registration_service_1.RegistrationService,
                locale_service_1.LocaleService
            ],
            imports: [
                forms_1.ReactiveFormsModule,
                forms_1.FormsModule,
                common_1.CommonModule,
                http_1.HttpModule
            ],
            exports: [
                registration_component_1.RegistrationComponent,
                profile_component_1.ProfileComponent
            ]
        })
    ], RegistrationModule);
    return RegistrationModule;
}());
exports.RegistrationModule = RegistrationModule;
//# sourceMappingURL=registration.module.js.map