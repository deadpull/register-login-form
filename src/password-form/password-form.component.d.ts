import { EventEmitter, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { ErrorService } from '../error.service';
export declare class PasswordFormComponent implements OnInit {
    private service;
    private errorService;
    autofocusInputRef: ElementRef;
    showPassword: boolean;
    showConfirm: boolean;
    passwords: FormGroup;
    registrationSuccessEnd: boolean;
    private password;
    verification_guid: string;
    msisdn: string;
    onClose: EventEmitter<void>;
    registrationSuccess: EventEmitter<string>;
    forgotPassEvent: EventEmitter<boolean>;
    keyboardInput(event: KeyboardEvent): void;
    constructor(service: RegistrationService, errorService: ErrorService);
    ngOnInit(): void;
    formControlValid(control: FormControl): boolean;
    closeClicked(): void;
    formValid(): boolean;
    passwordFormChange(): void;
    registerButtonClick(): void;
    getErrorText(formControlName: any): any;
    getErrorParams(formControlName: any): any;
    toggleShow(field: any): void;
    areEqual(group: FormGroup): {
        areEqual: boolean;
    };
}
