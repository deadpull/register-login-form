import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-registration-success',
  template: `
    <div class="reg-success-message">
      <div class="reg-success-img"><img src="assets/images/check_small.svg" alt=""></div>
      <div class="reg-form-info">{{'register_success_congratulations' | translate}} {{systemName}}! </div>
    </div>
`
})
export class RegistrationSuccessComponent {
  @Input() systemName: string;
}
