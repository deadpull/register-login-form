import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { IUser, IUserCheckResult } from '../interfaces';
export declare class LoginFormComponent implements OnInit {
    private service;
    autofocusInputRef: ElementRef;
    passwordField: AbstractControl;
    wrongPassword: boolean;
    showPass: boolean;
    registrationSuccess: boolean;
    private password;
    msisdn: string;
    verification_guid: string;
    user: EventEmitter<IUser>;
    forgotPassEvent: EventEmitter<boolean>;
    userCredentials: EventEmitter<IUserCheckResult>;
    keyboardInput(event: KeyboardEvent): void;
    constructor(service: RegistrationService);
    ngOnInit(): void;
    togglePassShow(): void;
    loginButtonClick(): void;
    forgotPass(): void;
    passwordInputKeyUp(event: any): void;
    passwordInputChange(): void;
}
