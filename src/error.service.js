"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var constants_1 = require("./constants");
var errorTranslateKeys = {
    password: {
        minlength: {
            text: 'error_min_length',
            params: constants_1.default.passwordMinLength
        },
        maxlength: {
            text: 'error_max_length',
            params: constants_1.default.passwordMaxLength
        },
        pattern: {
            text: 'error_wrong_symbols'
        }
    }
};
var ErrorService = /** @class */ (function () {
    function ErrorService() {
    }
    ErrorService.prototype.getError = function (formControl, type) {
        var errorFound = false;
        var currentError = '';
        for (var key in formControl.errors || {}) {
            if (!errorFound) {
                currentError = key;
                errorFound = true;
            }
        }
        return this.getErrorTextKey(currentError, type);
    };
    ErrorService.prototype.getErrorTextKey = function (error, type) {
        if (!error) {
            return null;
        }
        return errorTranslateKeys[type][error];
    };
    ErrorService = __decorate([
        core_1.Injectable()
    ], ErrorService);
    return ErrorService;
}());
exports.ErrorService = ErrorService;
//# sourceMappingURL=error.service.js.map