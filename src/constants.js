"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    passwordMinLength: 6,
    passwordMaxLength: 16,
    passwordRegexp: '^[a-zA-Z0-9]*$',
    otpMaxLength: 6
};
//# sourceMappingURL=constants.js.map