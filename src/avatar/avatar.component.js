"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
;
var AvatarComponent = /** @class */ (function () {
    function AvatarComponent() {
        this.editable = true;
        this.error = '';
        this.options = {
            maxWidth: 640,
            maxHeight: 640
        };
        this.onChange = new core_1.EventEmitter();
    }
    AvatarComponent.prototype.selectAvatar = function () {
        var _this = this;
        if (!this.editable) {
            return;
        }
        this.openFileDialog({ accept: 'image/*' })
            .map(function (data) { return data.target.result; })
            .flatMap(function (buffer) {
            if (!/^data\:image/.test(buffer)) {
                return Observable_1.Observable.of(null);
            }
            var img = new Image();
            setTimeout(function () { return img.src = buffer; });
            return Observable_1.Observable.fromEvent(img, 'load')
                .map(function () { return (img.width <= _this.options.maxWidth
                && img.height <= _this.options.maxHeight) ? buffer : null; });
        })
            .subscribe(function (buffer) { return _this.onChange.emit(buffer); });
    };
    AvatarComponent.prototype.openFileDialog = function (options) {
        if (options === void 0) { options = {}; }
        var inputElement = document.createElement('input');
        inputElement.type = 'file';
        inputElement.accept = options.accept || '';
        setTimeout(function () { return inputElement.click(); });
        return Observable_1.Observable.fromEvent(inputElement, 'change')
            .flatMap(function (event) {
            var file = inputElement.files[0];
            if (!file) {
                return Observable_1.Observable.throw({ message: 'invalid file' });
            }
            var reader = new FileReader();
            setTimeout(function () { return reader.readAsDataURL(file); });
            return Observable_1.Observable.fromEvent(reader, 'load');
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], AvatarComponent.prototype, "src", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], AvatarComponent.prototype, "editable", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], AvatarComponent.prototype, "error", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AvatarComponent.prototype, "options", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], AvatarComponent.prototype, "onChange", void 0);
    AvatarComponent = __decorate([
        core_1.Component({
            selector: 'app-avatar',
            template: "\n    <div class=\"avatar\" (click)=\"selectAvatar()\">\n      <div *ngIf=\"editable\" class=\"avatar-overlay\"></div>\n      <div *ngIf=\"error\" class=\"avatar-error-overlay\">\n        <div class=\"message\">{{error | translate}}</div>\n      </div>\n      <img *ngIf=\"!src\" src=\"../assets/images/blue-user.svg\" alt=\"\">  \n      <img *ngIf=\"src\" [src]=\"src\" alt=\"\">  \n    </div>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], AvatarComponent);
    return AvatarComponent;
}());
exports.AvatarComponent = AvatarComponent;
//# sourceMappingURL=avatar.component.js.map