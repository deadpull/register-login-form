import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RegistrationService } from "../registration.service";
export declare class ForgotFormComponent implements OnInit {
    private service;
    autofocusInputRef: ElementRef;
    otpMaxLength: number;
    showPass: boolean;
    wrongOTP: boolean;
    verification_attempts: number;
    OTPCodeInput: AbstractControl;
    verification_guid: string;
    otpVerified: EventEmitter<boolean>;
    keyboardInput(event: KeyboardEvent): void;
    constructor(service: RegistrationService);
    OTPChange(): void;
    togglePassShow(): void;
    readonly attempts: string;
    sendOtp(): void;
    ngOnInit(): void;
}
