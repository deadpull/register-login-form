"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var registration_service_1 = require("../registration.service");
var LoginFormComponent = /** @class */ (function () {
    function LoginFormComponent(service) {
        this.service = service;
        this.wrongPassword = false;
        this.showPass = false;
        this.registrationSuccess = false;
        this.user = new core_1.EventEmitter();
        this.forgotPassEvent = new core_1.EventEmitter();
        this.userCredentials = new core_1.EventEmitter();
    }
    LoginFormComponent.prototype.keyboardInput = function (event) {
        if (event.keyCode === 13) {
            this.loginButtonClick();
        }
    };
    LoginFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.passwordField = new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.minLength(3)]);
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    LoginFormComponent.prototype.togglePassShow = function () {
        this.showPass = !this.showPass;
    };
    LoginFormComponent.prototype.loginButtonClick = function () {
        var _this = this;
        if (!this.passwordField.valid) {
            this.wrongPassword = true;
            return;
        }
        this.service.login(this.passwordField.value, this.msisdn)
            .subscribe(function (data) {
            if (!data.wrongPassword) {
                _this.registrationSuccess = true;
                return _this.user.emit(data);
            }
            else {
                _this.wrongPassword = data.wrongPassword;
            }
        });
    };
    LoginFormComponent.prototype.forgotPass = function () {
        this.forgotPassEvent.emit(true);
    };
    LoginFormComponent.prototype.passwordInputKeyUp = function (event) {
        if (event.keyCode !== 13) {
            this.wrongPassword = false;
        }
    };
    LoginFormComponent.prototype.passwordInputChange = function () {
        this.password = '';
        if (this.passwordField.valid) {
            this.password = this.passwordField.value;
        }
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], LoginFormComponent.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginFormComponent.prototype, "msisdn", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginFormComponent.prototype, "verification_guid", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], LoginFormComponent.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], LoginFormComponent.prototype, "forgotPassEvent", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], LoginFormComponent.prototype, "userCredentials", void 0);
    __decorate([
        core_1.HostListener('window:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], LoginFormComponent.prototype, "keyboardInput", null);
    LoginFormComponent = __decorate([
        core_1.Component({
            selector: 'app-login-form',
            template: "\n    <div class=\"reg-form-wrapper reg-login-form\">\n      <div class=\"reg-register-form__img-holder\">\n        <div class=\"reg-main-img\"><img src=\"../assets/images/blue-user.svg\" alt=\"\"></div>\n        <div class=\"reg-icon-img\"><!--<img src=\"../assets/images/phone.svg\" alt=\"\">--></div>\n      </div>\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': wrongPassword}\">\n        <input\n          id=\"password\"\n          [formControl]=\"passwordField\"\n          autofocus\n          [type]=\"showPass ? 'text' : 'password'\"\n          (change)=\"passwordInputChange()\"\n          (keyup)=\"passwordInputKeyUp($event)\"\n          required\n          #autofocusInput\n        >\n        <label for=\"password\">{{'password' | translate}}</label>\n        <span class=\"reg-pass-icon\" (click)=\"togglePassShow()\">\n        <img [src]=\"showPass ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'\" alt=\"\">\n    </span>\n        <div class=\"reg-error-field\" *ngIf=\"wrongPassword\">\n          {{'password_wrong' | translate}}\n        </div>\n      </div>\n      <div class=\"reg-form-info\">\n        <span>{{'enter_password_request' | translate }}</span>\n        <button type=\"button\" class=\"reg-form-forgot\" (click)=\"forgotPass()\">{{'forgot_password_request' | translate }}</button>\n      </div>\n    </div>\n    <div class=\"controls\">\n      <button *ngIf=\"!registrationSuccess\" class=\"reg-button\" [disabled]=\"!passwordField.value\" (click)=\"loginButtonClick()\">\n        {{'login' | translate }}\n      </button>\n      <div *ngIf=\"registrationSuccess\" class=\"success-stats\">\n        <img src=\"../assets/images/check-white.svg\" alt=\"\t\u2713\">\n      </div>\n    </div>\n  ",
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService])
    ], LoginFormComponent);
    return LoginFormComponent;
}());
exports.LoginFormComponent = LoginFormComponent;
//# sourceMappingURL=login-form.component.js.map