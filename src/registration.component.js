"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var registration_service_1 = require("./registration.service");
var locale_service_1 = require("./locale.service");
var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(service, localeService) {
        this.service = service;
        this.localeService = localeService;
        this.user = new core_1.EventEmitter();
        this.registrationDone = new core_1.EventEmitter();
        this.onClose = new core_1.EventEmitter();
        this.extraOptions = {};
        this.locales = {};
        this.actions = {
            login: 'login',
            forgot: 'forgot',
            register: 'register',
            none: 'none'
        };
        this.userExists = false;
        this.userCheckDone = false;
        this.forgotPassword = false;
    }
    RegistrationComponent.prototype.getActionTitle = function () {
        if (!this.userExists) {
            return 'registration_in';
        }
        else {
            return 'enter_to';
        }
    };
    RegistrationComponent.prototype.forgotPassWord = function (e) {
        var _this = this;
        var userCheckResults$ = this.service.forgotPassGetUser(this.msisdn);
        userCheckResults$.subscribe(function (results) {
            _this.userCheckDone = true;
            if (!results) {
                _this.userExists = false;
                return;
            }
            _this.forgotPassword = e;
            _this.userCredentials = results;
            _this.user_guie = results.verification_guid;
            _this.userExists = results.userExists;
        });
    };
    RegistrationComponent.prototype.ngOnInit = function () {
        this.service.config = this.config;
        this.localeService.setLocale(this.lang);
        this.localeService.updateLocales(this.locales);
    };
    RegistrationComponent.prototype.ngOnChanges = function () {
        this.service.config = this.config;
        this.localeService.setLocale(this.lang);
    };
    RegistrationComponent.prototype.setUserAction = function (userExists) {
        this.userExists = userExists;
    };
    RegistrationComponent.prototype.getAction = function () {
        if (this.userCheckDone || this.extraOptions.userCheckDone) {
            if (this.registrationSuccess || this.extraOptions.registrationSuccess) {
            }
            if (this.forgotPassword) {
                return this.actions.register;
            }
            return (this.userExists || this.extraOptions.userExists) ? this.actions.login : this.actions.register;
        }
        else {
            return this.actions.none;
        }
    };
    Object.defineProperty(RegistrationComponent.prototype, "verificationGuid", {
        get: function () {
            return this.userCredentials && this.userCredentials.verification_guid;
        },
        enumerable: true,
        configurable: true
    });
    RegistrationComponent.prototype.otpVerification = function (verified) {
        this.otpVerified = verified;
    };
    RegistrationComponent.prototype.checkRegistrationSuccess = function (password) {
        var _this = this;
        this.registrationSuccess = true;
        var user$ = this.service.login(password, this.msisdn);
        user$.first().subscribe(function (data) {
            if (!data.wrongPassword) {
                setTimeout(function () { return _this.user.emit(data); }, 2000);
            }
        });
        if (this.registrationSuccess) {
            setTimeout(function () { return _this.registrationDone.emit(true); }, 2000);
        }
    };
    RegistrationComponent.prototype.getUserCredentials = function (userData) {
        this.userCheckDone = true;
        this.userCredentials = userData;
    };
    RegistrationComponent.prototype.getMsisdn = function (msisdn) {
        this.msisdn = msisdn;
    };
    RegistrationComponent.prototype.getUser = function (user) {
        if (user) {
            this.user.emit(__assign({}, user, { msisdn: this.msisdn }));
        }
    };
    RegistrationComponent.prototype.closeClicked = function () {
        this.onClose.emit();
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], RegistrationComponent.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], RegistrationComponent.prototype, "registrationDone", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], RegistrationComponent.prototype, "action", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], RegistrationComponent.prototype, "lang", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], RegistrationComponent.prototype, "user_guie", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], RegistrationComponent.prototype, "systemName", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], RegistrationComponent.prototype, "onClose", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], RegistrationComponent.prototype, "extraOptions", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], RegistrationComponent.prototype, "locales", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], RegistrationComponent.prototype, "config", void 0);
    RegistrationComponent = __decorate([
        core_1.Component({
            selector: 'registration',
            template: "\n    <div class=\"reg-register-form\">\n      <div class=\"reg-register-form__title\">{{getActionTitle() | translate}} {{systemName}}</div>\n      <!--<span class=\"reg-register-form__close\" (click)=\"closeClicked()\"></span>-->\n\n      <div [ngSwitch]=\"getAction()\" class=\"reg-register-form__actions-holder\">\n\n        <div *ngSwitchCase=\"actions.none\">\n          <app-phone-form\n            [lang]=\"lang\"\n            [action]=\"action\"\n            (userExistsEvent)=\"setUserAction($event)\"\n            (userCredentials)=\"getUserCredentials($event)\"\n            (phone)=\"getMsisdn($event)\" class=\"form-holder\">\n          </app-phone-form>\n        </div>\n\n        <div *ngSwitchCase=\"actions.login\">\n          <app-login-form class=\"form-holder\" [msisdn]=\"msisdn\"\n                          (user)=\"getUser($event)\"\n                          (forgotPassEvent)=\"forgotPassWord($event)\"\n                          [verification_guid]=\"verificationGuid\">\n            \n          </app-login-form>\n        </div>\n\n        <div class=\"reg-register-form__actions-block\" *ngSwitchCase=\"actions.register\">\n          <app-otp-form class=\"form-holder\" *ngIf=\"!otpVerified && !(extraOptions?.otpVerified)\"\n                        (otpVerified)=\"otpVerification($event)\"\n                        (forgotPassEvent)=\"forgotPassWord($event)\"\n                        [verification_guid]=\"verificationGuid\">\n          </app-otp-form>\n          <div class=\"reg-actions-block__new-password\" *ngIf=\"otpVerified || extraOptions?.otpVerified\">\n            <app-password-form\n              [msisdn]=\"msisdn\" [verification_guid]=\"verificationGuid\"\n              (registrationSuccess)=\"checkRegistrationSuccess($event)\" (forgotPassEvent)=\"forgotPassWord($event)\" class=\"form-holder\"></app-password-form>\n          </div>\n        </div>\n        <button class=\"cancel\" (click)=\"closeClicked()\">{{'cancel' | translate}}</button>\n\n      </div>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService,
            locale_service_1.LocaleService])
    ], RegistrationComponent);
    return RegistrationComponent;
}());
exports.RegistrationComponent = RegistrationComponent;
//# sourceMappingURL=registration.component.js.map