"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var mask_worker_1 = require("./mask.worker");
function getFinalSelectionPosition(pos) {
    if (pos <= 6) {
        return 7;
    }
    var defaultPosition = pos;
    switch (pos) {
        case 9:
            defaultPosition = defaultPosition + 2;
            break;
        case 14:
            defaultPosition++;
            break;
        case 17:
            defaultPosition++;
            break;
    }
    return defaultPosition;
}
function formatPhone(value) {
    if (value === void 0) { value = ''; }
    var maskWorker = new mask_worker_1.MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]');
    return maskWorker.useMask(value);
}
exports.formatPhone = formatPhone;
var PhoneDirective = /** @class */ (function () {
    function PhoneDirective(model, el) {
        var _this = this;
        this.model = model;
        this.el = el;
        this.valueChanges = new core_1.EventEmitter();
        this.maskWorker = new mask_worker_1.MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]', function (string) {
            // на случай, если пользователь пытается ввести телефон начиная с символов (3\8\0), которые разрешены маской,
            // но низя
            if (string.length === 7) {
                if (/[0]/.test(string[6])) {
                    return string.slice(0, 6);
                }
            }
            return string;
        });
        this.valueChanges.subscribe(function (phone) {
            if (_this.formControl) {
                _this.formControl.setValue(phone, {
                    onlySelf: true,
                    emitEvent: false,
                    emitModelToViewChange: false,
                    emitViewToModelChange: false
                });
            }
        });
    }
    PhoneDirective.prototype.ngOnInit = function () {
        if (this.formControl) {
            this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
        }
    };
    PhoneDirective.prototype.onInputChange = function (event) {
        var start = this.el.nativeElement.selectionStart;
        var maskedValue = this.maskWorker.useMask(event);
        this.model.valueAccessor.writeValue(maskedValue);
        this.el.nativeElement.selectionStart = getFinalSelectionPosition(start);
        this.el.nativeElement.selectionEnd = getFinalSelectionPosition(start);
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.AbstractControl)
    ], PhoneDirective.prototype, "formControl", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PhoneDirective.prototype, "valueChanges", void 0);
    PhoneDirective = __decorate([
        core_1.Directive({
            selector: '[phone]',
            providers: [forms_1.NgModel],
            host: {
                '(ngModelChange)': 'onInputChange($event)'
            }
        }),
        __metadata("design:paramtypes", [forms_1.NgModel, core_1.ElementRef])
    ], PhoneDirective);
    return PhoneDirective;
}());
exports.PhoneDirective = PhoneDirective;
//# sourceMappingURL=phone.directive.js.map