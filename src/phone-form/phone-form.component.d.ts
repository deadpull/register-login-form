import { EventEmitter, ElementRef, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { IUserCheckResult } from '../interfaces';
export declare class PhoneFormComponent implements OnInit {
    private service;
    autofocusInputRef: ElementRef;
    action: string;
    lang: string;
    phone: EventEmitter<string>;
    userCredentials: EventEmitter<IUserCheckResult>;
    userExistsEvent: EventEmitter<boolean>;
    keyboardInput(event: KeyboardEvent): void;
    userCheckDone: boolean;
    private _userExists;
    userExists: any;
    showError: boolean;
    userData: IUserCheckResult;
    phoneInput: AbstractControl;
    private latestValue;
    constructor(service: RegistrationService);
    ngOnInit(): void;
    sendOtp(): void;
    enableErrorField(): boolean;
    valueChange(): void;
    checkUserExists(number: string): void;
    confirmUser(): void;
}
