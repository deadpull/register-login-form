import { EventEmitter, OnInit, ElementRef } from '@angular/core';
import { NgModel, AbstractControl } from "@angular/forms";
export declare function formatPhone(value?: string): any;
export declare class PhoneDirective implements OnInit {
    private model;
    private el;
    private maskWorker;
    formControl: AbstractControl;
    valueChanges: EventEmitter<string>;
    constructor(model: NgModel, el: ElementRef);
    ngOnInit(): void;
    onInputChange(event: any): void;
}
