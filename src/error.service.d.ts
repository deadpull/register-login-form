import { AbstractControl } from "@angular/forms";
export declare class ErrorService {
    getError(formControl: AbstractControl, type: string): any;
    getErrorTextKey(error: string, type: string): any;
}
