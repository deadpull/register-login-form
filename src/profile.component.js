"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var registration_service_1 = require("./registration.service");
var locale_service_1 = require("./locale.service");
var mask_worker_1 = require("./mask.worker");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(service, localeService) {
        this.service = service;
        this.localeService = localeService;
        this.locales = {};
        this.options = {
            buttons: {
                editAvatar: true,
                editInfo: true,
                editPassword: true
            }
        };
        this.hidden = false;
        this.avatarSrc = '';
        this.wrongPassword = false;
        this.avatarError = '';
        this.onLogout = new core_1.EventEmitter();
        this.onClose = new core_1.EventEmitter();
        this.onUserUpdate = new core_1.EventEmitter();
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.localeService.setLocale(this.lang);
        this.localeService.updateLocales(this.locales);
        setTimeout(function () {
            _this.service.config = _this.config;
            _this.formatPhone();
        });
    };
    ProfileComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        this.localeService.setLocale(this.lang);
        setTimeout(function () {
            _this.service.config = _this.config;
            _this.avatarSrc = _this.service.getUserAvatar(_this.user);
            _this.formatPhone();
        });
    };
    ProfileComponent.prototype.changePassword = function () {
        this.state = 'changePassword';
    };
    ProfileComponent.prototype.edit = function () {
        this.state = 'edit';
    };
    ProfileComponent.prototype.logout = function () {
        this.user = null;
        this.onLogout.emit();
    };
    ProfileComponent.prototype.close = function () {
        this.state = '';
        this.hidden = true;
        this.onClose.emit();
    };
    ProfileComponent.prototype.editProfile = function (data) {
        var _this = this;
        this.service.editUser(this.user, data)
            .subscribe(function (updatedData) {
            if (!updatedData) {
                return;
            }
            _this.onUserUpdate.emit(updatedData);
            _this.state = '';
        }, function (res) {
            if (data.photo && res.status === 413) {
                _this.showAvatarSizeError();
            }
            else {
                _this.wrongPassword = true;
            }
        });
    };
    ProfileComponent.prototype.formatPhone = function () {
        if (!this.user) {
            return;
        }
        var msisdn = this.user.msisdn;
        var _value = msisdn.slice(0, 8) + '***' + msisdn.slice(-2);
        var maskWorker = new mask_worker_1.MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9|\*]');
        this.msisdn = maskWorker.useMask(_value);
    };
    ProfileComponent.prototype.onAvatarChange = function (data) {
        if (!data) {
            this.showAvatarSizeError();
            return;
        }
        this.avatarError = '';
        this.editProfile({ photo: data });
    };
    ProfileComponent.prototype.showAvatarSizeError = function () {
        var _this = this;
        this.avatarError = 'avatar_size_too_large';
        setTimeout(function () { return _this.avatarError = ''; }, 2 * 1000);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "user", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ProfileComponent.prototype, "lang", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "config", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "locales", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "options", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProfileComponent.prototype, "onLogout", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProfileComponent.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProfileComponent.prototype, "onUserUpdate", void 0);
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'mosst-profile',
            template: "\n    <div class=\"reg-register-form\" *ngIf=\"user\">\n      <div class=\"reg-register-form__title\">{{'profile' | translate}}</div>\n      <div class=\"reg-register-form__actions-holder profile-view-content\">\n        <div *ngIf=\"!state\" class=\"reg-register-form__actions-holder\">\n          <div>\n            <div class=\"reg-form-wrapper reg-phone-form\">\n              <div class=\"reg-register-form__img-holder\">\n                <div class=\"reg-main-img\">\n                  <app-avatar\n                    [src]=\"avatarSrc\"\n                    [error]=\"avatarError\"\n                    [editable]=\"!options?.buttons || options?.buttons?.editAvatar !== false\"\n                    (onChange)=\"onAvatarChange($event)\"\n                  ></app-avatar>\n                </div>\n              </div>\n      \n              <h3>{{user.first_name}} {{user.last_name}}</h3>\n              <h4 phone>{{msisdn}}</h4>\n              <br>\n      \n              <div>\n                <button *ngIf=\"!options?.buttons || options?.buttons?.editInfo !== false\"\n                  class=\"profile-view-button change-pass\"\n                  type=\"button\"\n                  (click)=\"edit()\"\n                >\n                  <i class=\"mdi mdi-account\"></i>\n                  {{'personal_info' | translate}}\n                </button>\n      \n                <button *ngIf=\"!options?.buttons || options?.buttons?.editPassword !== false\"\n                  class=\"profile-view-button change-pass\"\n                  type=\"button\"\n                  (click)=\"changePassword()\"\n                >\n                  <i class=\"mdi mdi-lock\"></i>\n                  {{'change_password' | translate}}\n                </button>\n      \n                <button class=\"profile-view-button\" type=\"button\" (click)=\"logout()\">\n                  <i class=\"mdi mdi-exit-to-app\"></i>\n                  {{'logout' | translate}}\n                </button>\n              </div>\n            </div>\n          </div>\n\n          <div>\n            <button class=\"profile-close-button\" (click)=\"close()\">{{'cancel' | translate}}</button>\n          </div>\n        </div>\n\n        <div *ngIf=\"state === 'changePassword'\">\n          <app-change-password-form\n            [wrongPassword]=\"wrongPassword\"\n            (onSubmit)=\"editProfile($event)\"\n            (onCancel)=\"state = ''\"\n          ></app-change-password-form>\n        </div>\n\n        <div *ngIf=\"state === 'edit'\">\n          <app-edit-profile-form\n            [user]=\"user\"\n            (onSubmit)=\"editProfile($event)\"\n            (onCancel)=\"state = ''\"\n          ></app-edit-profile-form>\n        </div>\n      </div>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService,
            locale_service_1.LocaleService])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map