import { OnInit, OnChanges, EventEmitter } from '@angular/core';
import { RegistrationService } from "./registration.service";
import { ILocales, IUser, IServiceConfig, IUserCheckResult } from './interfaces';
import { LocaleService } from './locale.service';
export interface IExtraOptions {
    userCheckDone?: boolean;
    registrationSuccess?: boolean;
    userExists?: boolean;
    otpVerified?: boolean;
    user_guie?: string;
    forgotPassword?: boolean;
}
export declare class RegistrationComponent implements OnInit, OnChanges {
    private service;
    private localeService;
    user: EventEmitter<IUser>;
    registrationDone: EventEmitter<boolean>;
    action: string;
    lang: string;
    user_guie: string;
    systemName: string;
    getActionTitle(): "registration_in" | "enter_to";
    onClose: EventEmitter<void>;
    extraOptions: IExtraOptions;
    locales: ILocales;
    config: IServiceConfig;
    msisdn: string;
    password: string;
    registrationSuccess: boolean;
    userCredentials: IUserCheckResult;
    otpVerified: boolean;
    actions: {
        login: string;
        forgot: string;
        register: string;
        none: string;
    };
    userExists: boolean;
    userCheckDone: boolean;
    forgotPassword: boolean;
    constructor(service: RegistrationService, localeService: LocaleService);
    forgotPassWord(e: boolean): void;
    ngOnInit(): void;
    ngOnChanges(): void;
    setUserAction(userExists: boolean): void;
    getAction(): string;
    readonly verificationGuid: string;
    otpVerification(verified: boolean): void;
    checkRegistrationSuccess(password: string): void;
    getUserCredentials(userData: IUserCheckResult): void;
    getMsisdn(msisdn: string): void;
    getUser(user: IUser): void;
    closeClicked(): void;
}
