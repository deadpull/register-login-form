import { Directive, EventEmitter, Input, Output, OnInit, ElementRef } from '@angular/core';
import { NgModel, AbstractControl } from "@angular/forms";
import { MaskWorker } from "./mask.worker";


export function formatPhone(value = '') {
  const maskWorker = new MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]');
  return maskWorker.useMask(value);
}

@Directive({
  selector: '[otp]',
  providers: [NgModel],
  host: {
    '(ngModelChange)': 'onInputChange($event)'
  }
})
export class OTPDirective implements OnInit {

  private maskWorker: MaskWorker;

  @Input() formControl: AbstractControl;

  @Output() valueChanges: EventEmitter<string> = new EventEmitter<string>();

  constructor(private model: NgModel, private el: ElementRef) {
    this.maskWorker = new MaskWorker('XXXXXX', 'XXXXXX', '[0-9]', (otpString) => {
      if (otpString.length > 6) {
        return otpString.slice(0, 6)
      }
      return otpString;
    });

    this.valueChanges.subscribe(phone => {
      if (this.formControl) {
        this.formControl.setValue(phone, {
          onlySelf: true,
          emitEvent: false,
          emitModelToViewChange: false,
          emitViewToModelChange: false
        });
      }
    });
  }

  ngOnInit() {
    if (this.formControl) {
      this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
    }
  }

  onInputChange(event) {
    const start = this.el.nativeElement.selectionStart;

    const maskedValue = this.maskWorker.useMask(event);
    this.model.valueAccessor.writeValue(maskedValue);

    this.el.nativeElement.selectionStart = start;
    this.el.nativeElement.selectionEnd = start;

    this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
  }

}
