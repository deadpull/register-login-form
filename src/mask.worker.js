"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MaskWorker = /** @class */ (function () {
    function MaskWorker(mask, output, allowedInputs, advancedSetter, replacement) {
        if (advancedSetter === void 0) { advancedSetter = null; }
        if (replacement === void 0) { replacement = 'X'; }
        this.mask = mask;
        this.output = output;
        this.allowedInputs = allowedInputs;
        this.advancedSetter = advancedSetter;
        this.replacement = replacement;
        this.allowedSymbols = new RegExp(allowedInputs);
    }
    MaskWorker.prototype.removeNotAllowedSymbolsFromString = function (inputString, ignoreReplacement) {
        if (inputString === void 0) { inputString = ''; }
        if (ignoreReplacement === void 0) { ignoreReplacement = false; }
        var stringWithoutNotAllowedSymbols = '';
        for (var i = 0; i < inputString.length; i++) {
            if (this.allowedSymbols.test(inputString[i]) || (ignoreReplacement && inputString[i] === this.replacement)) {
                stringWithoutNotAllowedSymbols += inputString[i];
            }
        }
        return stringWithoutNotAllowedSymbols;
    };
    MaskWorker.prototype.restoreToTextOutput = function (string) {
        var meaningPart = this.getMeaningPart(string);
        var result = this.output;
        for (var i = 0; i < meaningPart.length; i++) {
            result = result.replace(/X/, meaningPart[i]);
        }
        result = result.replace(/X/g, '');
        return result;
    };
    MaskWorker.prototype.getMeaningPart = function (string) {
        var meaningPart = '';
        if (string.length > this.mask.length) {
            string = string.slice(0, this.mask.length);
        }
        for (var i = 0; i < string.length; i++) {
            if (this.mask[i] == 'X') {
                meaningPart += string[i];
            }
        }
        return meaningPart;
    };
    MaskWorker.prototype.removePredefinedOutput = function (string) {
        var getFirstMatchTail = function (string, symbol) {
            for (var i = 0; i < string.length; i++) {
                if (string[i] === symbol) {
                    return string.slice(i);
                }
            }
            return '';
        };
        var equalPartsLength = function (string1, string2) {
            var length = 0;
            for (var i = 0; i < string1.length; i++) {
                if (string1[i] == string2[i]) {
                    length++;
                }
                else {
                    return length;
                }
            }
            return length;
        };
        var outputTemplate = this.removeNotAllowedSymbolsFromString(this.output);
        var outputTail = getFirstMatchTail(outputTemplate, string[0]);
        return string.slice(equalPartsLength(outputTail, string));
    };
    MaskWorker.prototype.cutTail = function (string) {
        var _this = this;
        var getPreviousReplacerIndex = function (index) {
            if (!index) {
                return 0;
            }
            for (var i = index - 1; i > 0; i--) {
                if (_this.mask[i] === _this.replacement) {
                    return i + 1;
                }
            }
            return index;
        };
        var re = new RegExp('X', 'g');
        var findReplacer = re.exec(string);
        return findReplacer ? string.slice(0, getPreviousReplacerIndex(findReplacer.index)) : string;
    };
    MaskWorker.prototype.useTextMask = function (string) {
        var allowedLength = this.mask.length - this.mask.replace(/X/g, '').length;
        var stringWithoutNotAllowedSymbols = this.removeNotAllowedSymbolsFromString(string);
        string = this.removePredefinedOutput(stringWithoutNotAllowedSymbols).slice(0, allowedLength);
        var currentString = this.mask;
        for (var i = 0; i < string.length; i++) {
            currentString = currentString.replace(/X/, string[i]);
        }
        currentString = this.cutTail(currentString);
        return currentString;
    };
    MaskWorker.prototype.applyAdvancedSetter = function (string) {
        if (typeof this.advancedSetter === 'function') {
            return this.advancedSetter(string);
        }
        if (this.advancedSetter instanceof RegExp) {
            return string.match(this.advancedSetter) ? string : '';
        }
        return string;
    };
    MaskWorker.prototype.useMask = function (string) {
        if (string === void 0) { string = ''; }
        string = string.toString();
        if (typeof this.mask === "function") {
            string = this.removeNotAllowedSymbolsFromString(string);
            return this.mask(string);
        }
        return this.useTextMask(this.applyAdvancedSetter(string));
    };
    MaskWorker.prototype.unmask = function (string) {
        if (string === void 0) { string = ''; }
        if (typeof this.output === "function") {
            return this.output(string);
        }
        return this.restoreToTextOutput(string);
    };
    return MaskWorker;
}());
exports.MaskWorker = MaskWorker;
//# sourceMappingURL=mask.worker.js.map