"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var registration_service_1 = require("../registration.service");
var constants_1 = require("../constants");
var error_service_1 = require("../error.service");
var PasswordFormComponent = /** @class */ (function () {
    function PasswordFormComponent(service, errorService) {
        this.service = service;
        this.errorService = errorService;
        this.showPassword = false;
        this.showConfirm = false;
        this.registrationSuccessEnd = false;
        this.onClose = new core_1.EventEmitter();
        this.registrationSuccess = new core_1.EventEmitter();
        this.forgotPassEvent = new core_1.EventEmitter();
        var validators = [forms_1.Validators.required,
            forms_1.Validators.minLength(constants_1.default.passwordMinLength),
            forms_1.Validators.maxLength(constants_1.default.passwordMaxLength),
            forms_1.Validators.pattern(constants_1.default.passwordRegexp)];
        this.passwords = new forms_1.FormGroup({
            newPassword: new forms_1.FormControl('', validators),
            repeat: new forms_1.FormControl('', validators)
        }, this.areEqual);
        this.passwords.valueChanges.subscribe(this.passwordFormChange.bind(this));
    }
    PasswordFormComponent.prototype.keyboardInput = function (event) {
        if (event.keyCode === 13) {
            if (this.passwords.valid) {
                this.registerButtonClick();
            }
        }
    };
    PasswordFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    PasswordFormComponent.prototype.formControlValid = function (control) {
        return !control.touched || !control.dirty;
    };
    PasswordFormComponent.prototype.closeClicked = function () {
        this.onClose.emit();
    };
    PasswordFormComponent.prototype.formValid = function () {
        return this.passwords.valid
            || this.formControlValid(this.passwords.controls['newPassword'])
            || this.formControlValid(this.passwords.controls['repeat']);
    };
    PasswordFormComponent.prototype.passwordFormChange = function () {
        this.password = '';
        if (this.passwords.valid) {
            this.password = this.passwords.controls['repeat'].value;
        }
    };
    PasswordFormComponent.prototype.registerButtonClick = function () {
        var _this = this;
        if (this.forgotPassEvent) {
            var user$ = this.service.registerNewPassForgot(this.password, this.msisdn, this.verification_guid);
            user$.subscribe(function (userRegistered) {
                _this.registrationSuccess.emit(_this.password);
                _this.registrationSuccessEnd = true;
            });
        }
        else {
            var user$ = this.service.register(this.password, this.msisdn, this.verification_guid);
            user$.subscribe(function (userRegistered) {
                _this.registrationSuccess.emit(_this.password);
                _this.registrationSuccessEnd = true;
            });
        }
    };
    PasswordFormComponent.prototype.getErrorText = function (formControlName) {
        var errorObject = this.errorService.getError(this.passwords.controls[formControlName], 'password');
        return errorObject && errorObject.text || '';
    };
    PasswordFormComponent.prototype.getErrorParams = function (formControlName) {
        var errorObject = this.errorService.getError(this.passwords.controls[formControlName], 'password');
        return errorObject && errorObject.params || '';
    };
    PasswordFormComponent.prototype.toggleShow = function (field) {
        if (field === 'confirm') {
            this.showConfirm = !this.showConfirm;
        }
        else if (field === 'password') {
            this.showPassword = !this.showPassword;
        }
    };
    PasswordFormComponent.prototype.areEqual = function (group) {
        var valid;
        var currentValue;
        for (var name_1 in group.controls) {
            if (group.controls.hasOwnProperty(name_1)) {
                if (!currentValue) {
                    currentValue = group.controls[name_1].value;
                }
                valid = currentValue === group.controls[name_1].value;
            }
        }
        if (valid) {
            return null;
        }
        return {
            areEqual: true
        };
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], PasswordFormComponent.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PasswordFormComponent.prototype, "verification_guid", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PasswordFormComponent.prototype, "msisdn", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PasswordFormComponent.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PasswordFormComponent.prototype, "registrationSuccess", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], PasswordFormComponent.prototype, "forgotPassEvent", void 0);
    __decorate([
        core_1.HostListener('window:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], PasswordFormComponent.prototype, "keyboardInput", null);
    PasswordFormComponent = __decorate([
        core_1.Component({
            selector: 'app-password-form',
            template: "\n    <div class=\"reg-form-wrapper reg-password-form\">\n      <div class=\"reg-register-form__img-holder\">\n        <div class=\"reg-icon-img\"><!--<img src=\"assets/images/phone.svg\" alt=\"\">--></div>\n        <div class=\"reg-icon-img\"><!--&gt;<img src=\"assets/images/message.svg\" alt=\"\">--></div>\n        <div class=\"reg-main-img\"><img src=\"assets/images/blue-lock.svg\" alt=\"\"></div>\n      </div>\n      <div class=\"reg-change-pass\" [formGroup]=\"passwords\">\n        <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !formValid()}\">\n          <input id=\"new-password\" formControlName=\"newPassword\" autofocus #autofocusInput [type]=\"showPassword ? 'text' : 'password'\"\n                 required>\n          <label for=\"new-password\">{{'set_password' | translate}}</label>\n          <span class=\"reg-pass-icon\" (click)=\"toggleShow('password')\">\n        <img [src]=\"showPassword ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'\" alt=\"\">\n        </span>\n        </div>\n        <div class=\"reg-error-field\" *ngIf=\"!formValid()\">\n          {{getErrorText('newPassword') | translate: getErrorParams('newPassword')}}\n        </div>\n        <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !formValid()}\">\n          <input id=\"repeat-password\" formControlName=\"repeat\" [type]=\"showConfirm ? 'text' : 'password'\" required>\n          <label for=\"repeat-password\">{{'repeat_password' | translate}}</label>\n          <span class=\"reg-pass-icon\" (click)=\"toggleShow('confirm')\">\n        <img [src]=\"showConfirm ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'\" alt=\"\">\n      </span>\n        </div>\n        <div class=\"reg-error-field\" *ngIf=\"!formValid()\">\n          {{getErrorText('repeat') | translate: getErrorParams('repeat')}}\n        </div>\n        <div class=\"reg-error-field\" *ngIf=\"!formValid()\">\n          {{'password_mismatch' | translate}}\n        </div>\n        <div class=\"reg-form-info\">{{'set_password_using_symbols' | translate}}</div>\n      </div>\n    </div>\n    <div class=\"controls\">\n      <button *ngIf=\"!registrationSuccessEnd\" class=\"reg-button\" [disabled]=\"!password\" (click)=\"registerButtonClick()\">{{'proceed' | translate}}</button>\n      <div *ngIf=\"registrationSuccessEnd\" class=\"success-stats\">\n        <img src=\"../assets/images/check-white.svg\" alt=\"\t\u2713\">\n      </div>\n    </div>\n  ",
        }),
        __metadata("design:paramtypes", [registration_service_1.RegistrationService, error_service_1.ErrorService])
    ], PasswordFormComponent);
    return PasswordFormComponent;
}());
exports.PasswordFormComponent = PasswordFormComponent;
//# sourceMappingURL=password-form.component.js.map