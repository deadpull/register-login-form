declare const _default: {
    passwordMinLength: number;
    passwordMaxLength: number;
    passwordRegexp: string;
    otpMaxLength: number;
};
export default _default;
