"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var EditProfileForm = /** @class */ (function () {
    function EditProfileForm() {
        this.onSubmit = new core_1.EventEmitter();
        this.onCancel = new core_1.EventEmitter();
        this.formGroup = new forms_1.FormGroup({
            firstName: new forms_1.FormControl('', forms_1.Validators.required),
            lastName: new forms_1.FormControl('', forms_1.Validators.required),
            middleName: new forms_1.FormControl(''),
            email: new forms_1.FormControl('', forms_1.Validators.pattern(/[a-zA-Z0-9_-]+@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)?/))
        });
    }
    EditProfileForm.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.initFormValues(); });
        setTimeout(function () { return _this.autofocusInputRef.nativeElement.focus(); }, 100);
    };
    EditProfileForm.prototype.ngOnChanges = function () {
        var _this = this;
        setTimeout(function () { return _this.initFormValues(); });
    };
    EditProfileForm.prototype.initFormValues = function () {
        this.formGroup.setValue({
            firstName: this.user ? this.user.first_name || '' : '',
            lastName: this.user ? this.user.last_name || '' : '',
            middleName: this.user ? this.user.middle_name || '' : '',
            email: this.user && this.user.emails ? this.user.emails[0] || '' : ''
        });
    };
    EditProfileForm.prototype.validateControl = function (key) {
        var control = this.formGroup.controls[key];
        return !control || !!(control.valid || !control.touched);
    };
    EditProfileForm.prototype.submit = function () {
        if (!this.formGroup.valid) {
            return;
        }
        var _a = this.formGroup.value, firstName = _a.firstName, lastName = _a.lastName, middleName = _a.middleName, email = _a.email;
        this.onSubmit.emit({
            first_name: firstName,
            last_name: lastName,
            middle_name: middleName || '',
            email: email
        });
    };
    EditProfileForm.prototype.close = function () {
        this.onCancel.emit();
    };
    __decorate([
        core_1.ViewChild('autofocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], EditProfileForm.prototype, "autofocusInputRef", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditProfileForm.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditProfileForm.prototype, "onSubmit", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditProfileForm.prototype, "onCancel", void 0);
    EditProfileForm = __decorate([
        core_1.Component({
            selector: 'app-edit-profile-form',
            template: "\n    <form class=\"reg-form-wrapper\" [formGroup]=\"formGroup\" (ngSubmit)=\"submit()\">\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !validateControl('firstName')}\">\n        <input\n          id=\"firstName\"\n          autocomplete=\"off\"\n          required\n          type=\"text\"\n          formControlName=\"firstName\"\n        >\n        <label for=\"firstName\">{{'first_name' | translate}}</label>\n      </div>\n\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !validateControl('lastName')}\">\n        <input\n          id=\"lastName\"\n          autocomplete=\"off\"\n          autofocus\n          required\n          type=\"text\"\n          formControlName=\"lastName\"\n          #autofocusInput\n        >\n        <label for=\"lastName\">{{'last_name' | translate}}</label>\n      </div>\n\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !validateControl('middleName')}\">\n        <input\n          id=\"middleName\"\n          autocomplete=\"off\"\n          type=\"text\"\n          formControlName=\"middleName\"\n        >\n        <label for=\"middleName\">{{'middle_name' | translate}}</label>\n      </div>\n\n      <div class=\"reg-input-holder reg-simple-field\" [ngClass]=\"{'error-input': !validateControl('email')}\">\n        <input\n          id=\"email\"\n          autocomplete=\"off\"\n          type=\"text\"\n          formControlName=\"email\"\n        >\n        <label for=\"email\">{{'email' | translate}}</label>\n      </div>\n      \n      <div class=\"button-block\">\n        <button type=\"button\" class=\"cancel--static\" (click)=\"close()\">{{'cancel' | translate}}</button>\n        <button type=\"submit\" class=\"reg-button\" [disabled]=\"!formGroup.valid\">{{'confirm' | translate}}</button>\n      </div>\n    </form>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], EditProfileForm);
    return EditProfileForm;
}());
exports.EditProfileForm = EditProfileForm;
//# sourceMappingURL=edit-profile-form.component.js.map