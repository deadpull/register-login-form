"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var userExistsMessage = 'profile exists';
var RegistrationService = /** @class */ (function () {
    function RegistrationService(http) {
        this.http = http;
    }
    RegistrationService.prototype.getUserAuthInfo = function (phoneNumber) {
        var url = this.config.registrationUrl;
        var method = 'POST';
        var body = { msisdn: phoneNumber, web: 1 };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            var result = { userExists: false };
            if (res && res.status === 200) {
                var body_1 = res.json();
                if (body_1.error) {
                    if (body_1.error === userExistsMessage) {
                        result.userExists = true;
                        return result;
                    }
                    return null;
                }
                result.verification_guid = body_1.verification.verification_guid;
                result.verification_attempts = body_1.verification.verification_attempts;
            }
            return result;
        }).catch(function (error, caught) {
            return Observable_1.Observable.of({});
        });
    };
    RegistrationService.prototype.forgotPassGetUser = function (phoneNumber) {
        var url = this.config.forgotUrl;
        var method = 'POST';
        var body = { msisdn: phoneNumber, web: 1 };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            var result = { userExists: false };
            if (res && res.status === 200) {
                var body_2 = res.json();
                if (body_2.error) {
                    if (body_2.error === userExistsMessage) {
                        result.userExists = true;
                        return result;
                    }
                    return null;
                }
                result.verification_guid = body_2.verification.verification_guid;
                result.verification_attempts = body_2.verification.verification_attempts;
            }
            return result;
        }).catch(function (error, caught) {
            return Observable_1.Observable.of({});
        });
    };
    RegistrationService.prototype.checkOTPCode = function (otpCode, verification_guid) {
        var url = this.config.otpCheckUrl;
        var method = 'POST';
        var body = { vvalue: otpCode, verification_guid: verification_guid };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            var result = {};
            if (res && res.status === 200) {
                var body_3 = res.json();
                if (body_3.error) {
                    return result;
                }
                if (body_3.verification) {
                    result.status = body_3.verification.status;
                    result.verification_attempts = body_3.verification.verification_attempts;
                }
            }
            return result;
        });
    };
    RegistrationService.prototype.login = function (password, msisdn) {
        var _this = this;
        var url = this.config.loginUrl;
        var method = 'POST';
        var body = { msisdn: msisdn, password: password };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            var result = {};
            if (res && res.status === 200) {
                var body_4 = res.json();
                if (body_4.error) {
                    throw new Error(body_4.error);
                }
                result = body_4;
            }
            return result;
        })
            .flatMap(function (user) { return _this.getProfile(user)
            .map(function (profile) { return Object.assign({}, user, profile); }); })
            .catch(function (err) {
            return Observable_1.Observable.of({ wrongPassword: true });
        });
    };
    RegistrationService.prototype.registerNewPassForgot = function (password, msisdn, verification_guid) {
        var url = this.config.forgotUrl;
        var method = 'POST';
        var body = { msisdn: msisdn, password: password, verification_guid: verification_guid, web: 1 };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            if (res && res.status === 200) {
                var body_5 = res.json();
                if (body_5 && body_5.profile == 'created') {
                    return true;
                }
                if (body_5.error) {
                    return false;
                }
            }
            return false;
        });
    };
    RegistrationService.prototype.register = function (password, msisdn, verification_guid) {
        var url = this.config.registrationUrl;
        var method = 'POST';
        var body = { msisdn: msisdn, password: password, verification_guid: verification_guid, web: 1 };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            if (res && res.status === 200) {
                var body_6 = res.json();
                if (body_6 && body_6.profile == 'created') {
                    return true;
                }
                if (body_6.error) {
                    return false;
                }
            }
            return false;
        });
    };
    RegistrationService.prototype.editUser = function (user, updatedata) {
        var profile_session_guid = user.profile_session_guid, profile_guid = user.profile_guid;
        var url = this.config.profileUrl;
        var method = 'POST';
        var body = { profile_session_guid: profile_session_guid, profile_guid: profile_guid, updatedata: updatedata };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            if (res && res.status === 200) {
                var json = res.json();
                if (!json.error) {
                    return Object.assign({}, json.profile, {
                        avatar_path: json.profile.avatar_guid
                    });
                }
                else {
                    throw new Error(json.message || json.error);
                }
            }
            return null;
        });
    };
    RegistrationService.prototype.getProfile = function (user) {
        var profile_session_guid = user.profile_session_guid, profile_guid = user.profile_guid;
        var url = this.config.profileUrl;
        var method = 'POST';
        var body = { profile_session_guid: profile_session_guid, profile_guid: profile_guid };
        var options = new http_1.RequestOptions({ url: url, method: method, body: body });
        return this.http.request(url, options)
            .map(function (res) {
            if (res && res.status === 200) {
                var json = res.json();
                if (!json.error) {
                    return Object.assign({}, json.profile, json.session, {
                        avatar_path: json.profile.avatar_guid
                    });
                }
                else {
                    throw new Error(json.message);
                }
            }
            return null;
        });
    };
    RegistrationService.prototype.getUserAvatar = function (user, size) {
        if (size === void 0) { size = 200; }
        var avatarUrl = this.config.avatarUrl;
        if (!avatarUrl) {
            return null;
        }
        var avatar_path = user.avatar_path;
        return avatar_path ? avatarUrl + "/" + avatar_path + "_" + size + ".jpg" : '';
    };
    RegistrationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], RegistrationService);
    return RegistrationService;
}());
exports.RegistrationService = RegistrationService;
//# sourceMappingURL=registration.service.js.map